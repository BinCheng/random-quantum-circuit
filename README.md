This repo contains my codes in simulating random quantum circuits. 
`random sampling method.ipynb` is based on [arXiv:1610.01808](https://arxiv.org/abs/1610.01808), but before I test it, I gave up this method.
`Effective Fourier Components.ipynb` is a new method that possibly will work. But it has some theoretical obstacles.

So then I changed to some more traditional methods. I have done some surveys, and found some exsiting algorithms. 
`path integral.ipynb` is based on [arXiv: 1710.09364](https://arxiv.org/abs/1710.09364) and [arXiv:1612.05903](https://arxiv.org/abs/1612.05903). The advantage of these algorithm is that they only require $`poly(n)`$ spaces, 
although the runtime is exponential on something.

Now my focus is on the trade-off between space and time. This project is still in progress.