import numpy as np
import qutip as qt
import sys
from time import clock

pi = np.pi
e = np.e
x = qt.sigmax()

#%%file random_circuit.py
def Circuit(n1, n2, d):   # create a random circuit with n1 \times n2 qubits and depth d according to 1608.00263
    circuit = {}
    single_gate = ["X_half", "Y_half", "T"]
    
    for m in range(d+1):
        circuit[m] = {} # circuit[0] is the Hadamard layer
        for m1 in range(n1):
            for m2 in range(n2):
                count = m1*n2 + m2   # the count-th qubit
                if m == 0:
                    circuit[m][count] = ["H", [m1, m2]]  # create the Hadamard layer
                    continue
                elif m%8 ==1:  # layer 1
                    if ((m1+1)%2 == 1 and (m2+1)%4 == 1 and (m2+1)!= n2) or ((m1+1)%2 == 0 and (m2+1)%4 == 3 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 1 and (m2+1)%4 == 2) or ((m1+1)%2 == 0 and (m2+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]   # "T" for target
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==2:  # layer 2
                    if ((m1+1)%2 == 0 and (m2+1)%4 == 1 and (m2+1)!= n2) or ((m1+1)%2 == 1 and (m2+1)%4 == 3 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 0 and (m2+1)%4 == 2) or ((m1+1)%2 == 1 and (m2+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==7:  # layer 7
                    if ((m2+1)%2 == 0 and (m1+1)%4 == 1 and (m1+1)!= n1) or ((m2+1)%2 == 1 and (m1+1)%4 == 3 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 0 and (m1+1)%4 == 2) or ((m2+1)%2 == 1 and (m1+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==0 and m != 0:  # layer 8
                    if ((m2+1)%2 == 1 and (m1+1)%4 == 1 and (m1+1)!= n1) or ((m2+1)%2 == 0 and (m1+1)%4 == 3 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 1 and (m1+1)%4 == 2) or ((m2+1)%2 == 0 and (m1+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==5:  # layer 5
                    if ((m2+1)%2 == 1 and (m1+1)%4 == 2 and (m1+1)!= n1) or ((m2+1)%2 == 0 and (m1+1)%4 == 0 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 1 and (m1+1)%4 == 3) or ((m2+1)%2 == 0 and (m1+1)%4 == 1 and (m1+1)>4):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==6:  # layer 6
                    if ((m2+1)%2 == 0 and (m1+1)%4 == 2 and (m1+1)!= n1) or ((m2+1)%2 == 1 and (m1+1)%4 == 0 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 0 and (m1+1)%4 == 3) or ((m2+1)%2 == 1 and (m1+1)%4 == 1 and (m1+1)>4):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==3:  # layer 3
                    if ((m1+1)%2 == 0 and (m2+1)%4 == 2 and (m2+1)!= n2) or ((m1+1)%2 == 1 and (m2+1)%4 == 0 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 0 and (m2+1)%4 == 3) or ((m1+1)%2 == 1 and (m2+1)%4 == 1 and (m2+1)>4):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==4:  # layer 4
                    if ((m1+1)%2 == 1 and (m2+1)%4 == 2 and (m2+1)!= n2) or ((m1+1)%2 == 0 and (m2+1)%4 == 0 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 1 and (m2+1)%4 == 3) or ((m1+1)%2 == 0 and (m2+1)%4 == 1 and (m2+1)>4):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
    
    return circuit


def Gate(gate, b1, b2):   
    if gate == "CZ":
        if b1 == b2 == 1:
            return -1
        else:
            return 1
    else:   
        up = qt.basis(2,0)
        b1_ket = x**(b1)*up  # input index
        b2_ket = x**(b2)*up  # output index
        if gate == "H":
            return (b1_ket.dag()*qt.snot()*b2_ket)[0,0]
        elif gate == "X_half":
            return (b1_ket.dag()*qt.rx(pi/2)*b2_ket)[0,0]
        elif gate == "Y_half":
            return (b1_ket.dag()*qt.ry(pi/2)*b2_ket)[0,0]
        elif gate == "T":
            return (b1_ket.dag()*qt.phasegate(pi/4)*b2_ket)[0,0]


def InitialFactors(circuit):
    '''
    create initial factors according to the circuit
    '''
    factors = []   # used to store all factors: factors = [temp1, temp2, ...] 
    
    for m2 in range(n2):
        for m1 in range(n1):
            for m in range(d+1):
                factor = np.zeros((2,2), dtype=complex)   # used to store a single factor
                temp = []   # used to store a single factor: temp = [factor, index1, index2, ...], 
                            # index is of the form [k,m]                
                k = m1*n2 + m2   # the k-th qubit
                gate = circuit[m][k]
                if gate[0] == "CZT":
                    continue
                    
                for b1 in range(2):
                    for b2 in range(2):
                        factor[b1,b2] = Gate(gate[0],b1,b2)
                temp.append(factor)
                
                count = 0
                try:   # count the number of CZ gates before the current gate                    
                    for m3 in range(m):
                        if circuit[m3][k][0] == "CZ" or circuit[m3][k][0] == "CZT":
                            count += 1
                except Exception:
                    pass
                temp.insert(1, [k,m-count])
                
                
                if gate[0] == "CZ":
                    k1 = n2*gate[2][0] + gate[2][1] 
                    count1 = 0
                    try:
                        for m3 in range(m):
                            if circuit[m3][k1][0] == "CZ" or circuit[m3][k1][0] == "CZT":
                                count1 += 1
                    except Exception:
                        pass
                    temp.insert(1, [k1,m-count1])
                else:
                    temp.insert(1, [k,m+1-count])

                factors.append(temp)
    return factors



def Binary2Decimal(binary_list):
    '''return a decimal number, given an input binary list'''
    result = 0
    for m in range(len(binary_list)):
        result += 2**m*binary_list[len(binary_list)-m-1]
    return int(result)




def Decimal2Binary(n,l):
    '''return a binary list of length l, given the decimal number n'''
    result = []
    while n//2 != 0:
        remainder = n%2
        result.insert(0, int(remainder))
        n = (n - remainder)/2
    result.insert(0,int(n))
    while len(result) < l:
        result.insert(0,0)
    return result



def List2ArrayElement(l, a):   
    '''
    convert a list into an array index, and then return the corresponding value. "l" for list, and "a" for array
    '''
    b = len(a.shape)      
    d = Binary2Decimal(l)   # convert the list l into a decimal number d
    return a.reshape(2**b)[d]



def Multiplication(fac1, fac2, var1, var2):
    '''
    multiply fac1 and fac2 to get a larger factor
    '''
    fac2.pop(var2+1)   # this pop operation might have problem in case of deep copy
    result = fac1 + fac2
    result.pop(len(fac1))
    size = len(result) - 1   # size of the new factor
    factor = np.zeros(2**size, dtype=complex)
    for m in range(2**size):
        b = Decimal2Binary(m, size)
        b1 = b[: (len(fac1) - 1)]
        b2 = b[(len(fac1)-1): ]
        b2 = np.insert(b2, var2, b1[var1])
        factor[m] = List2ArrayElement(b1, fac1[0])*List2ArrayElement(b2.tolist(), fac2[0])
    a = 2*np.ones(size, dtype=int)
    factor = factor.reshape(a)
    result[0] = factor
    #print(size)
    #print(a)
    return result



def VariableElimination(fac, var):
    '''
    Eliminate the var-th variable of fac[0] by summing over it. fac is of the form [array, [j,k],[j',k']].
    '''
    size = len(fac)-2   # size of the new factor
    
    factor = np.zeros(2**size, dtype=complex)
    for m in range(2**size):
        for m1 in range(2):
            d = Decimal2Binary(m, size)
            d = np.insert(d, var, m1)
            factor[m] += List2ArrayElement(d.tolist(), fac[0])
    a = 2*np.ones(size, dtype=int)
    factor = factor.reshape(a)
    fac[0] = factor
    fac.pop(var+1)
    return fac



def VariableEliminationAssign(fac, var, b):
    '''
    Eliminate the var-th variable of fac[0] by assigning the value b to it. fac is of the form [array, [j,k],[j',k']].
    '''
    size = len(fac)-2   # size of the new factor
    
    factor = np.zeros(2**size, dtype=complex)
    for m in range(2**size):
        d = Decimal2Binary(m, size)
        d = np.insert(d, var, b)
        factor[m] += List2ArrayElement(d.tolist(), fac[0])
    a = 2*np.ones(size, dtype=int)
    factor = factor.reshape(a)
    fac[0] = factor
    fac.pop(var+1)
    return fac


def Merge(fac, var1, var2):
    '''
    Elimination duplicate variables of a factor.
    '''
    size = len(fac) - 2   # size of the new factor
    
    factor = np.zeros(2**size, dtype=complex)
    for m in range(2**size):
        d = Decimal2Binary(m, size)
        b = d[var1]
        d = np.insert(d, var2, b)
        factor[m] += List2ArrayElement(d.tolist(), fac[0])
    a = 2*np.ones(size, dtype=int)
    factor = factor.reshape(a)
    fac[0] = factor
    fac.pop(var2+1)
    return fac


def VerticleOrdering(circuit, init, final):   # V for verticle ordering
    factors = InitialFactors(circuit)   # used to store all factors: factors = [temp1, temp2, ...], where temp = [factor, [j,k], [j',k'], ...]
    result = 1    
    
    depth = np.zeros(n, dtype=int)
    for m1 in range(len(factors)):
        for m2 in range(len(factors[m1]) - 1):
            index = factors[m1][m2+1]
            if depth[index[0]] < index[1]:
                depth[index[0]] = index[1]   # find the depth of every qubit
    #print(depth)
    
    #print(factors, "\n")
    while len(factors) != 0:
        #print(factors)
        #print(result)
        #print("\n")
        index1 = factors[0][1]   # index1 if [j,k], j-th qubit and k-th timestep

        if index1[1] == 0:
            b1 = init[index1[0]]
            if len(factors[0]) == 2:
                result = result*factors[0][0][b1]
                factors.pop(0)
                continue
            factors[0] = VariableEliminationAssign(factors[0], 0, b1)
            continue
        #print(factors, "\n")
        l = len(factors)-1
        m = 1
        #depth = index1[1]
        while m <= l:
            check = False
            for m1 in range(len(factors[m])-1):                
                if index1 == factors[m][m1+1]:
                    #print("m = ", m, " ,m1 = ", m1)
                    factors[0] = Multiplication(factors[0], factors[m], 0, m1)
                    factors.pop(m)
                    #print(factors, "\n")
                    
                    m2 = 1
                    l2 = len(factors[0]) - 2
                    while m2 < l2:
                        check1 = True
                        index2 = factors[0][m2+1]
                        for m3 in range(l2-m2):
                            index3 = factors[0][l2+1-m3]
                            if index2 == index3:
                                factors[0] = Merge(factors[0], m2, l2-m3)
                                l2 -= 1
                                check1 = False
                                break
                        if check1:
                            m2 += 1
                                
                    l -= 1
                    break
                if m1 == len(factors[m]) - 2:
                    check = True
                #if index1[0] == factors[m][m1+1][0] and depth < factors[m][m1+1][1]:
                #    depth = factors[m][m1+1][1]   # find the depth of the worldline of the current qubit
            if check == True:
                m += 1
        #print("\n", m, l)
        #print("before elimination\n", factors, "\n")
        #for m1 in range(len(factors[0])-1):
        #    if index1[0] == factors[0][m1+1][0] and depth < factors[0][m1+1][1]:
        #        depth = factors[0][m1+1][1]   # find the depth of the worldline of the current qubit
        
        #print("qubit:", index1[0], ", depth:", depth)
        if depth[index1[0]] == index1[1]:
            #print(index1)
            b2 = final[index1[0]]
            if len(factors[0]) == 2:
                result = result*factors[0][0][b2]
                factors.pop(0)
            else:
                factors[0] = VariableEliminationAssign(factors[0], 0, b2)
                #factors[0][0] = factors[0][0][b2]
                #(factors[0]).pop(1)
        else:
            if len(factors[0]) == 2:
                result = result*(factors[0][0][0] + factors[0][0][1])
                factors.pop(0)
            else:
                factors[0] = VariableElimination(factors[0], 0)
        #print(factors, "\n")
    return result





#with open("variable_elimination.txt", "w+", newline = "\r\n") as f:
#	f.write("\r\n")


#for n1 in [3,4,5]:
#	for n2 in [2,3,4,5]:
#		for d in range(20):
#for n1 in [3,4,5]:
#    for n2 in [5]:
#        for d in range(20):
#            time = []
#            for times in range(3):


for n1 in [4,5]:
    for d in range(11):
        t = clock()
        n2 = 5
        n = n1*n2

        circuit = Circuit(n1,n2,d+1)
        init = np.random.choice(1,n) # initial state
        final = np.random.choice(2,n) # intended output state
        VerticleOrdering(circuit, init, final)

        t = clock() - t
        #                if t >= 2000:
        #                    break
        #                time.append(t)
        #            if t >= 2000:
        #                break
        #            t = sum(time)/3

        with open("variable_elimination.txt", "a+", newline = "\r\n") as f:
            f.write("n1: %s ," %n1)
            f.write("n2: %s ," %n2)
            f.write("d: %s ," %(d+1))
            f.write("%s\r\n" %t)