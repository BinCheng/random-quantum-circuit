import numpy as np
import qutip as qt
from time import clock

pi = np.pi
e = np.e
x = qt.sigmax()

def Circuit(n,d):  # generate the circuit randomly
    circuit = {}
    count = 0
    
    for m in range(n):
        count += 1
        circuit[count] = ['H', m] # the first layer of Hadamard gates
    
    for m in range(d):
        for m1 in range(n):
            count += 1
            indicator = np.random.choice(3)
            if indicator == 0:
                circuit[count] = ["H",m1] # Hadamard acting on the m1-th qubit
            elif indicator == 1:
                circuit[count] = ["T",m1]  # T-gate acting on the m1-th qubit
            else: # note that n has to be greater than 1
                if m1 == n-1:
                    circuit[count] = ["CZ",n-2,n-1] # CZ acting on the (n-1)-th and (n-2)-th qubit
                else:
                    circuit[count] = ["CZ",m1,m1+1] # CZ acting on the m1-th and (m1 + 1)-th qubit
    return circuit

for m in range(3):
	for m1 in range(19):
		t = clock()

		n = m1 + 2
		d = n
		circuit = Circuit(n,d)
		init = np.zeros(n)
		final = np.random.choice(2,n)
		
		up = qt.basis(2,0) # |0>
		for m2 in range(n):
		    if m2 == 0:
		        initQ = x**(init[m2])*up
		        finalq = x**(final[m2])*up
		        continue
		    initQ = qt.tensor(initQ, x**(init[m2])*up)
		    finalq = qt.tensor(finalq, x**(final[m2])*up)

		finalQ = initQ

		for m2 in range(n*d+n):
		    gate = circuit[m2+1][0]
		    if gate == 'H':
		        finalQ = qt.snot(n,circuit[m2+1][1])*finalQ
		    elif gate == 'T':
		        finalQ = qt.phasegate(pi/4,n,circuit[m2+1][1])*finalQ
		    else:
		        finalQ = qt.csign(n,circuit[m2+1][1],circuit[m2+1][2])*finalQ

		t = clock() - t

		with open("matrix-vector.txt", "a+", newline = "\r\n") as f:
			f.write("%s " %n)
			f.write("%s\r\n" %t)