def Circuit(n1, n2, d):   # create a random circuit with n1 \times n2 qubits and depth d
    circuit = {}
    single_gate = ["X_half", "Y_half", "T"]
    
    for m in range(d+1):
        circuit[m] = {} # circuit[0] is the Hadamard layer
        count = 0
        for m1 in range(n1):
            for m2 in range(n2):
                count += 1
                if m == 0:
                    circuit[m][count] = ["H", [m1, m2]]  # create the Hadamard layer  
                elif m%8 ==1:  # layer 1
                    if ((m1+1)%2 == 1 and (m2+1)%4 == 1) or ((m1+1)%2 == 0 and (m2+1)%4 == 3):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 1 and (m2+1)%4 == 2) or ((m1+1)%2 == 0 and (m2+1)%4 == 0):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==2:  # layer 2
                    if ((m1+1)%2 == 0 and (m2+1)%4 == 1) or ((m1+1)%2 == 1 and (m2+1)%4 == 3):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 0 and (m2+1)%4 == 2) or ((m1+1)%2 == 1 and (m2+1)%4 == 0):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==3:  # layer 3
                    if ((m2+1)%2 == 0 and (m1+1)%4 == 1) or ((m2+1)%2 == 1 and (m1+1)%4 == 3):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 0 and (m1+1)%4 == 2) or ((m2+1)%2 == 1 and (m1+1)%4 == 0):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==4:  # layer 4
                    if ((m2+1)%2 == 1 and (m1+1)%4 == 1) or ((m2+1)%2 == 0 and (m1+1)%4 == 3):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 1 and (m1+1)%4 == 2) or ((m2+1)%2 == 0 and (m1+1)%4 == 0):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==5:  # layer 5
                    if ((m2+1)%2 == 1 and (m1+1)%4 == 2) or ((m2+1)%2 == 0 and (m1+1)%4 == 0 and (m1+1)!=n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 1 and (m1+1)%4 == 3) or ((m2+1)%2 == 0 and (m1+1)%4 == 1 and (m1+1)>4):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==6:  # layer 6
                    if ((m2+1)%2 == 0 and (m1+1)%4 == 2) or ((m2+1)%2 == 1 and (m1+1)%4 == 0 and (m1+1)!=n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 0 and (m1+1)%4 == 3) or ((m2+1)%2 == 1 and (m1+1)%4 == 1 and (m1+1)>4):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==7:  # layer 7
                    if ((m1+1)%2 == 0 and (m2+1)%4 == 2) or ((m1+1)%2 == 1 and (m2+1)%4 == 0 and (m2+1)!=n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 0 and (m2+1)%4 == 3) or ((m1+1)%2 == 1 and (m2+1)%4 == 1 and (m2+1)>4):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==0 and m != 0:  # layer 8
                    if ((m1+1)%2 == 1 and (m2+1)%4 == 2) or ((m1+1)%2 == 0 and (m2+1)%4 == 0 and (m2+1)!=n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 1 and (m2+1)%4 == 3) or ((m1+1)%2 == 0 and (m2+1)%4 == 1 and (m2+1)>4):
                        continue
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
    
    return circuit