import numpy as np
#import qutip as qt
from time import clock

pi = np.pi
e = np.e
#x = qt.sigmax()

def Circuit(n,d):  # generate the circuit randomly
    circuit = {}
    count = 0
    
    for m in range(n):
        count += 1
        circuit[count] = ['Hadamard', m] # the first layer of Hadamard gates
    
    for m in range(d):
        for m1 in range(n):
            count += 1
            indicator = np.random.choice(3)
            if indicator == 0:
                circuit[count] = ["Hadamard",m1] # Hadamard acting on the m1-th qubit
            elif indicator == 1:
                circuit[count] = ["T",m1]  # T-gate acting on the m1-th qubit
            else: # note that n has to be greater than 1
                if m1 == n-1:
                    circuit[count] = ["CZ",n-2,n-1] # CZ acting on the (n-1)-th and (n-2)-th qubit
                else:
                    circuit[count] = ["CZ",m1,m1+1] # CZ acting on the m1-th and (m1 + 1)-th qubit
    return circuit

def Final_S(init, final, circuit, d): # This is based on Shi's Algorithm (1710.09364)
    stack = [[init,1]]             # stack[][1] record the level where current state is in
    phase = [1]
    final_amp = 0
    count = 0

    while len(stack) != 0:
        count = stack[0][1] # the current level of the tree
        current = stack[0][0] # current state
        if count == n*d + 1: # if it reaches the final level of the tree
            if (current == final).all():
                final_amp += phase[0]
            stack.pop(0)
            phase.pop(0)
            continue

        gate = circuit[count]    # the corresponding gate
        if gate[0] == 'CZ':
            q1 = gate[1]
            q2 = gate[2]
            amp = (-1)**(current[q1]*current[q2])*phase[0]
            phase[0] = amp
            stack[0][1] = count + 1
        elif gate[0] == 'T':
            q1 = gate[1]
            amp = e**(1j*pi*current[q1]/4)*phase[0]
            phase[0] = amp
            stack[0][1] = count + 1
        elif gate[0] == "Hadamard":
            q1 = gate[1]
            current1 = current.copy()
            current1[q1] = 0
            current2 = current.copy()
            current2[q1] = 1
            amp1 = phase[0]/np.sqrt(2)
            amp2 = (-1)**(current[q1])*phase[0]/np.sqrt(2)

            stack.pop(0)
            phase.pop(0)
            stack.insert(0,[current1,count+1])
            phase.insert(0,amp1)
            stack.insert(0,[current2,count+1])
            phase.insert(0,amp2)
    return final_amp


for m in range(3):
	for m1 in range(19):
		t = clock()

		n = m1 + 2
		d = n
		circuit = Circuit(n,d)
		init = np.zeros(n)
		final = np.random.choice(2,n)
		Final_S(init,final,circuit, d+1)

		t = clock() - t

		with open("path-integral.txt", "a+", newline = "\r\n") as f:
			f.write("%s " %n)
			f.write("%s\r\n" %t)