import numpy as np
import qutip as qt
import sys
from time import clock

pi = np.pi
e = np.e
x = qt.sigmax()

#%%file random_circuit.py
def Circuit(n1, n2, d):   # create a random circuit with n1 \times n2 qubits and depth d according to 1608.00263
    circuit = {}
    single_gate = ["X_half", "Y_half", "T"]
    
    for m in range(d+1):
        circuit[m] = {} # circuit[0] is the Hadamard layer
        for m1 in range(n1):
            for m2 in range(n2):
                count = m1*n2 + m2   # the count-th qubit
                if m == 0:
                    circuit[m][count] = ["H", [m1, m2]]  # create the Hadamard layer
                    continue
                elif m%8 ==1:  # layer 1
                    if ((m1+1)%2 == 1 and (m2+1)%4 == 1 and (m2+1)!= n2) or ((m1+1)%2 == 0 and (m2+1)%4 == 3 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 1 and (m2+1)%4 == 2) or ((m1+1)%2 == 0 and (m2+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]   # "T" for target
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==2:  # layer 2
                    if ((m1+1)%2 == 0 and (m2+1)%4 == 1 and (m2+1)!= n2) or ((m1+1)%2 == 1 and (m2+1)%4 == 3 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 0 and (m2+1)%4 == 2) or ((m1+1)%2 == 1 and (m2+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==7:  # layer 7
                    if ((m2+1)%2 == 0 and (m1+1)%4 == 1 and (m1+1)!= n1) or ((m2+1)%2 == 1 and (m1+1)%4 == 3 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 0 and (m1+1)%4 == 2) or ((m2+1)%2 == 1 and (m1+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==0 and m != 0:  # layer 8
                    if ((m2+1)%2 == 1 and (m1+1)%4 == 1 and (m1+1)!= n1) or ((m2+1)%2 == 0 and (m1+1)%4 == 3 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 1 and (m1+1)%4 == 2) or ((m2+1)%2 == 0 and (m1+1)%4 == 0):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==5:  # layer 5
                    if ((m2+1)%2 == 1 and (m1+1)%4 == 2 and (m1+1)!= n1) or ((m2+1)%2 == 0 and (m1+1)%4 == 0 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 1 and (m1+1)%4 == 3) or ((m2+1)%2 == 0 and (m1+1)%4 == 1 and (m1+1)>4):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==6:  # layer 6
                    if ((m2+1)%2 == 0 and (m1+1)%4 == 2 and (m1+1)!= n1) or ((m2+1)%2 == 1 and (m1+1)%4 == 0 and (m1+1)!= n1):
                        circuit[m][count] = ["CZ", [m1, m2], [m1+1, m2]]
                    elif ((m2+1)%2 == 0 and (m1+1)%4 == 3) or ((m2+1)%2 == 1 and (m1+1)%4 == 1 and (m1+1)>4):
                        circuit[m][count] = ["CZT", [m1-1, m2], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==3:  # layer 3
                    if ((m1+1)%2 == 0 and (m2+1)%4 == 2 and (m2+1)!= n2) or ((m1+1)%2 == 1 and (m2+1)%4 == 0 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 0 and (m2+1)%4 == 3) or ((m1+1)%2 == 1 and (m2+1)%4 == 1 and (m2+1)>4):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
                elif m%8 ==4:  # layer 4
                    if ((m1+1)%2 == 1 and (m2+1)%4 == 2 and (m2+1)!= n2) or ((m1+1)%2 == 0 and (m2+1)%4 == 0 and (m2+1)!= n2):
                        circuit[m][count] = ["CZ", [m1, m2], [m1, m2+1]]
                    elif ((m1+1)%2 == 1 and (m2+1)%4 == 3) or ((m1+1)%2 == 0 and (m2+1)%4 == 1 and (m2+1)>4):
                        circuit[m][count] = ["CZT", [m1, m2-1], [m1, m2]]
                    else:
                        circuit[m][count] = [np.random.choice(single_gate,1)[0], [m1, m2]]
    
    return circuit

#with open("state_evolution.txt", "a+", newline = "\r\n") as f:
#    f.write("\r\n")

#for n1 in [3,4,5]:
#   for n2 in [2,3,4,5]:
n1 = 5
n2 = 5
for d in [5,6,7,8,9]:
#    time = []
#    for times in range(3):
    t = clock()
    n = n1*n2

    circuit = Circuit(n1,n2,d+1)
    init = np.random.choice(1,n) # initial state
    final = np.random.choice(2,n) # intended output state

    # generate initial state and final state
    up = qt.basis(2,0) # |0>
    for m in range(n):
        if m == 0:
            initQ = x**(init[m])*up
            finalq = x**(final[m])*up
            continue
        initQ = qt.tensor(initQ, x**(init[m])*up)
        finalq = qt.tensor(finalq, x**(final[m])*up)


    finalQ = initQ

    for m in circuit:
        for key in circuit[m]:
            if circuit[m][key][0] == "H":
                a = n2*circuit[m][key][1][0] + circuit[m][key][1][1] # m1*n2+m2
                finalQ = qt.snot(n, a)*finalQ
            elif circuit[m][key][0] == "X_half":
                a = n2*circuit[m][key][1][0] + circuit[m][key][1][1] # m1*n2+m2
                finalQ = qt.rx(pi/2, n, a)*finalQ
            elif circuit[m][key][0] == "Y_half":
                a = n2*circuit[m][key][1][0] + circuit[m][key][1][1] # m1*n2+m2
                finalQ = qt.ry(pi/2, n, a)*finalQ
            elif circuit[m][key][0] == "T":
                a = n2*circuit[m][key][1][0] + circuit[m][key][1][1] # m1*n2+m2
                finalQ = qt.phasegate(pi/4, n, a)*finalQ
            elif circuit[m][key][0] == "CZ":
                a = n2*circuit[m][key][1][0] + circuit[m][key][1][1]
                b = n2*circuit[m][key][2][0] + circuit[m][key][2][1]
                finalQ = qt.csign(n, a, b)*finalQ

    t = (clock() - t)
#    time.append(t)

#    t = sum(time)/3

    with open("state_evolution.txt", "a+", newline = "\r\n") as f:
        f.write("n1: %s ," %n1)
        f.write("n2: %s ," %n2)
        f.write("d: %s ," %(d+1))
        f.write("%s\r\n" %t)
